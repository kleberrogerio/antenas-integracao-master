# Laboratório de Projeto de Banco de Dados

Este repositório é utilizado para mostrar as entregas para a Disciplina de Projeto de Bando de Dados. 

### Alunos
- Cristiano Gregio
- Kleber Rogério do Nascimento
- Maria Giovana Silva Realino

### Professor
- Eduardo Sakaue


## Entregas


|   | Entregas                             | Resultado          |
|---|--------------------------------------|--------------------|
| 1 | Deploy da Aplicação                  |         OK         |
| 2 | Utilização do Docker                 |         OK         |
| 3 | Instalação do Mongo                  |         OK         |
| 4 | Dividir ambientes: Dev, Stage e Prod | Em desenvolvimento |
| 5 | Integração com Jenkins               | Em desenvolvimento |
| 6 | Integração com Gerrit                | Em desenvolvimento |
| 7 | Automoção do MongoDB                 | Em desenvolvimento |



O deploy da aplicação Antenas foi implementado e pode ser acessado em: http://165.227.80.192:8081/

Board do Trello: https://trello.com/b/398kn2c3/lab-proj-banco-de-dados-antenas


